package be.sofico.bamboo.plugins.dynatrace;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.google.common.collect.Maps;
import com.opensymphony.xwork2.TextProvider;

/**
 * Configuration for the register test session task
 * 
 * @author jasa
 *
 */
public class RegisterTestSessionTaskConfigurator extends AbstractTaskConfigurator {

	private TextProvider textProvider;
	
    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull final ActionParametersMap params, @Nullable final TaskDefinition previousTaskDefinition) {
    	
        final Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
        
        config.put(DynatraceConfigurator.DT_VERSIONMAJOR, params.getString(DynatraceConfigurator.DT_VERSIONMAJOR));
        config.put(DynatraceConfigurator.DT_VERSIONMINOR, params.getString(DynatraceConfigurator.DT_VERSIONMINOR));
        config.put(DynatraceConfigurator.DT_VERSIONREVISION, params.getString(DynatraceConfigurator.DT_VERSIONREVISION));
        config.put(DynatraceConfigurator.DT_PLATFORM, params.getString(DynatraceConfigurator.DT_PLATFORM));
        config.put(DynatraceConfigurator.DT_CATEGORY, params.getString(DynatraceConfigurator.DT_CATEGORY));
        
        return config;
    }
    
    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context) {
    	
    	populateTestCategoryList(context);
    	
        context.put(DynatraceConfigurator.DT_VERSIONMAJOR, "");
        context.put(DynatraceConfigurator.DT_VERSIONMINOR, "");
        context.put(DynatraceConfigurator.DT_VERSIONREVISION, "");
        context.put(DynatraceConfigurator.DT_PLATFORM, System.getProperty("os.name"));
        context.put(DynatraceConfigurator.DT_CATEGORY, "webapi");
        
    }
    
    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition) {
    	
    	populateTestCategoryList(context);
    	
        context.put(DynatraceConfigurator.DT_VERSIONMAJOR, taskDefinition.getConfiguration().get(DynatraceConfigurator.DT_VERSIONMAJOR));
        context.put(DynatraceConfigurator.DT_VERSIONMINOR, taskDefinition.getConfiguration().get(DynatraceConfigurator.DT_VERSIONMINOR));
        context.put(DynatraceConfigurator.DT_VERSIONREVISION, taskDefinition.getConfiguration().get(DynatraceConfigurator.DT_VERSIONREVISION));
        context.put(DynatraceConfigurator.DT_PLATFORM, taskDefinition.getConfiguration().get(DynatraceConfigurator.DT_PLATFORM));
        context.put(DynatraceConfigurator.DT_CATEGORY, taskDefinition.getConfiguration().get(DynatraceConfigurator.DT_CATEGORY));
    }
    
    @Override
    public void populateContextForView(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition) {  
        
    	populateTestCategoryList(context);
    	
        context.put(DynatraceConfigurator.DT_VERSIONMAJOR, taskDefinition.getConfiguration().get(DynatraceConfigurator.DT_VERSIONMAJOR));
        context.put(DynatraceConfigurator.DT_VERSIONMINOR, taskDefinition.getConfiguration().get(DynatraceConfigurator.DT_VERSIONMINOR));
        context.put(DynatraceConfigurator.DT_VERSIONREVISION, taskDefinition.getConfiguration().get(DynatraceConfigurator.DT_VERSIONREVISION));
        context.put(DynatraceConfigurator.DT_PLATFORM, taskDefinition.getConfiguration().get(DynatraceConfigurator.DT_PLATFORM));
        context.put(DynatraceConfigurator.DT_CATEGORY, taskDefinition.getConfiguration().get(DynatraceConfigurator.DT_CATEGORY));
    }
    
    @Override
    public void validate(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection) {
    	
        super.validate(params, errorCollection);

    	checkRequiredValue(params, errorCollection, DynatraceConfigurator.DT_CATEGORY);
    }
    
    /**
     * Check required field
     * 
     * @param params
     * @param errorCollection
     * @param fieldName
     */
	private void checkRequiredValue(final ActionParametersMap params, final ErrorCollection errorCollection, String fieldName) {
		
		if(StringUtils.isEmpty(params.getString(fieldName))){
			String errLabelName = fieldName + ".error";
			String text = textProvider.getText(errLabelName);
			if(text==null){
				text = getI18nBean().getText(errLabelName);
			}
			errorCollection.addError(fieldName, text!=null?text:"Please provide a value for the field.");
		}
	}
	
    public void setTextProvider(final TextProvider textProvider) {
        
    	if (this.textProvider==null) {     
            this.textProvider=textProvider;
        }
    }
    
    private void populateTestCategoryList(@NotNull Map<String, Object> context) {

        // Key definitions by ID/Name
        Map<String, String> categoryList = Maps.newLinkedHashMap();
        categoryList.put("unit", "Unit Test");
        categoryList.put("uidriven", "UI-Driven Test");
        categoryList.put("performance", "Performance Test");
        categoryList.put("load", "Load Test");
        categoryList.put("webapi", "Web API Test");
        
        context.put("dtTestCategories", categoryList);        
    }
}
