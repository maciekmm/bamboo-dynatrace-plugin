package be.sofico.bamboo.plugins.dynatrace;

import java.util.Map;

import be.sofico.bamboo.plugins.dynatrace.util.BambooDynatraceConfiguration;

import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;

/**
 * Condition to control when the Dynatrace tab is displayed on the plan overview
 * 
 * @author jasa
 *
 */
public class DynatraceShowPlanTabCondition implements Condition {
	
	public static final String BUILD_KEY = "buildKey";
	
	private PlanManager planManager;

	@Override
	public void init(Map<String, String> params) throws PluginParseException {

	}

	@Override
	public boolean shouldDisplay(Map<String, Object> context) {
		
        final String buildKey = (String) context.get(BUILD_KEY);
        if (buildKey == null) {
            return false;
        }

        final Plan build = planManager.getPlanByKey(buildKey);
        if (build == null) {
            return false;
        }

        // Are we enabled
        BambooDynatraceConfiguration config = new BambooDynatraceConfiguration(build.getBuildDefinition().getCustomConfiguration());
        return config.isEnabled();
	}
	
    public void setPlanManager(PlanManager planManager) {
        this.planManager = planManager;
    }
}
