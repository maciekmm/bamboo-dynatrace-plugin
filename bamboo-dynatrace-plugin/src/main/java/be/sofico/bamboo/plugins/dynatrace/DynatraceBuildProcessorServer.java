package be.sofico.bamboo.plugins.dynatrace;

import be.sofico.bamboo.plugins.dynatrace.util.BambooDynatraceConfiguration;
import be.sofico.bamboo.plugins.dynatrace.util.BambooDynatraceUtil;
import be.sofico.bamboo.plugins.dynatrace.util.TestRunData;

import com.atlassian.bamboo.build.BuildLoggerManager;
import com.atlassian.bamboo.build.CustomBuildProcessorServer;
import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.builder.BuildState;
import com.atlassian.bamboo.v2.build.BaseConfigurableBuildPlugin;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.util.concurrent.NotNull;
import com.opensymphony.xwork2.TextProvider;

/**
 * Build processor which runs on the server once the build is completed, use it to fail the build when performance regressions are detected
 * 
 * @author jasa
 *
 */
public class DynatraceBuildProcessorServer extends BaseConfigurableBuildPlugin implements CustomBuildProcessorServer {
	
    
	private BuildContext buildContext;
    private TextProvider textProvider;
    private BuildLoggerManager buildLoggerManager;
    
    // Fixed delay of 10 seconds to wait for dynatrace data to become available
    private int delay = 10;

    public void init(@NotNull final BuildContext buildContext) {
        this.buildContext = buildContext;
    }

    @NotNull
	@Override
	public BuildContext call() throws InterruptedException, Exception {
    	
    	// Fetch the data for the current build
        BambooDynatraceConfiguration config = new BambooDynatraceConfiguration(buildContext.getParentBuildContext().getBuildDefinition().getCustomConfiguration());
        
        // Are we enabled
        if(!config.isEnabled()) {
        	return buildContext;
        }
        
        // Get logger
        BuildLogger logger = buildLoggerManager.getLogger(buildContext.getResultKey());
    	
        // Get the build number to retrieve the matching data for our testrun
        int buildNumber = buildContext.getBuildNumber();
        logger.addBuildLogEntry("DynatraceBuildProcessorServer - Interpreting results for build number: " + buildNumber);
        
        // Wait a tad to allow dynatrace data to become available
		if (delay != 0) {
			logger.addBuildLogEntry("DynatraceBuildProcessorServer - Sleeping for the configured delay of " + delay + "sec");
			Thread.sleep(delay*1000);
		}
        
		// Pull test data
		TestRunData runData = BambooDynatraceUtil.getTestRunForBuild(config, buildNumber, logger);
		int attemps = 6;
		while(runData==null && (attemps--)>0) {
			
			// Sleep
			logger.addBuildLogEntry("DynatraceBuildProcessorServer - Data not yet available, sleeping again for the configured delay of " + delay + "sec");
			Thread.sleep(delay*1000);
			
			// Pull data again
			runData = BambooDynatraceUtil.getTestRunForBuild(config, buildNumber, logger);
		} 
		
		// Final check to process data
		if(runData!=null) {
			
			logger.addBuildLogEntry("DynatraceBuildProcessorServer - Obtained test run data, evaluating response");
		
			// Fail the build when needed        
	        if(config.getFailBuilds() && (runData.getNumDegraded()>0 || runData.getNumFailed()>0)) {
	            buildContext.getBuildResult().setBuildState(BuildState.FAILED);
	            buildContext.getBuildResult().getBuildErrors().add(textProvider.getText(DynatraceConfigurator.DT_FAILBUILDS_MESSAGE));
	        }
		
		} else {
			
			logger.addBuildLogEntry("DynatraceBuildProcessorServer - Did not receive any run data (after retrying), unable to interpret build result");
		}
        
		return buildContext;
	}

    public void setTextProvider(final TextProvider textProvider) {
        
    	if (this.textProvider==null) {     
            this.textProvider=textProvider;
        }
    }
    
    public void setBuildLoggerManager(BuildLoggerManager buildLoggerManager) {
		this.buildLoggerManager = buildLoggerManager;
	}
    
    public BuildLoggerManager getBuildLoggerManager() {
		return buildLoggerManager;
	}
}
