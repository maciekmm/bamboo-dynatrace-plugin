package be.sofico.bamboo.plugins.dynatrace;

import java.util.Map;

import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.TopLevelPlan;
import com.atlassian.bamboo.v2.build.BaseBuildConfigurationAwarePlugin;
import com.atlassian.bamboo.v2.build.configuration.MiscellaneousBuildConfigurationPlugin;
import com.atlassian.bamboo.ww2.actions.build.admin.create.BuildConfiguration;

/**
 * Handle the plan level configuration for the dynatrace plugin
 * 
 * @author jasa
 *
 */
public class DynatraceConfigurator extends BaseBuildConfigurationAwarePlugin implements MiscellaneousBuildConfigurationPlugin {

	public static final String DT_ENABLE = "custom.be.sofico.bamboo.plugins.dynatrace.enable";
	public static final String DT_HOST = "custom.be.sofico.bamboo.plugins.dynatrace.host";
	public static final String DT_PORT = "custom.be.sofico.bamboo.plugins.dynatrace.port";
	public static final String DT_SECURE = "custom.be.sofico.bamboo.plugins.dynatrace.secure";
	public static final String DT_USER = "custom.be.sofico.bamboo.plugins.dynatrace.user";
	public static final String DT_PASSWORD = "custom.be.sofico.bamboo.plugins.dynatrace.password";
	public static final String DT_PROFILE = "custom.be.sofico.bamboo.plugins.dynatrace.profile";
	public static final String DT_FAILBUILDS = "custom.be.sofico.bamboo.plugins.dynatrace.failbuilds";
	public static final String DT_DEMO = "custom.be.sofico.bamboo.plugins.dynatrace.demo";
	
	public static final String DT_VERSIONMAJOR = "custom.be.sofico.bamboo.plugins.dynatrace.version.major";
	public static final String DT_VERSIONMINOR = "custom.be.sofico.bamboo.plugins.dynatrace.version.minor";
	public static final String DT_VERSIONREVISION = "custom.be.sofico.bamboo.plugins.dynatrace.version.revision";
	public static final String DT_CATEGORY = "custom.be.sofico.bamboo.plugins.dynatrace.category";
	public static final String DT_PLATFORM = "custom.be.sofico.bamboo.plugins.dynatrace.platform";
	
	public static final String DT_FAILBUILDS_MESSAGE = "dt-test-automation-configuration.failbuilds.message";
    
	
	@Override
	public boolean isApplicableTo(Plan plan) {

		if (plan instanceof TopLevelPlan)
			return true;

		return false;
	}

	@Override
	protected void populateContextForEdit(Map<String, Object> context, BuildConfiguration buildConfiguration, Plan plan) {
		
		// Put Enable setting
		context.put(DT_ENABLE, buildConfiguration.getBoolean(DT_ENABLE));
		context.put(DT_HOST, buildConfiguration.getString(DT_HOST));
		context.put(DT_PORT, buildConfiguration.getString(DT_PORT));
		context.put(DT_USER, buildConfiguration.getString(DT_USER));
		context.put(DT_PASSWORD, buildConfiguration.getString(DT_PROFILE));
		context.put(DT_FAILBUILDS, buildConfiguration.getBoolean(DT_FAILBUILDS));
		context.put(DT_SECURE, buildConfiguration.getBoolean(DT_SECURE));
		context.put(DT_DEMO, buildConfiguration.getBoolean(DT_DEMO));
	}
	
	@Override
	protected void populateContextForView(Map<String, Object> context, Plan plan) {
		
		Map<String, String> customConfiguration = plan.getBuildDefinition().getCustomConfiguration();
		
		// Put Enable setting
		context.put(DT_ENABLE, customConfiguration.get(DT_ENABLE));
		context.put(DT_HOST, customConfiguration.get(DT_HOST));
		context.put(DT_PORT, customConfiguration.get(DT_PORT));
		context.put(DT_USER, customConfiguration.get(DT_USER));
		context.put(DT_PASSWORD, customConfiguration.get(DT_PASSWORD));
		context.put(DT_FAILBUILDS, customConfiguration.get(DT_FAILBUILDS));
		context.put(DT_SECURE, customConfiguration.get(DT_SECURE));
		context.put(DT_DEMO, customConfiguration.get(DT_DEMO));
	}
}
