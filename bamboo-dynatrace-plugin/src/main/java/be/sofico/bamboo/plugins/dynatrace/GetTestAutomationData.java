package be.sofico.bamboo.plugins.dynatrace;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.jdom.JDOMException;

import be.sofico.bamboo.plugins.dynatrace.util.BambooDynatraceConfiguration;
import be.sofico.bamboo.plugins.dynatrace.util.BambooDynatraceUtil;
import be.sofico.bamboo.plugins.dynatrace.util.TestRunData;

import com.atlassian.bamboo.build.PlanResultsAction;
import com.opensymphony.webwork.dispatcher.json.JSONException;
import com.opensymphony.webwork.dispatcher.json.JSONObject;

/**
 * Retrieve the test automation data from dynatrace and convert it to JSON
 * 
 * @author jasa
 *
 */
public class GetTestAutomationData extends PlanResultsAction {

	private static final long serialVersionUID = 1L;

	private boolean isJob;
	
	/**
	 * Are we a Job or not
	 */
    public String doExecute() throws Exception {
    	
        String result = super.doExecute();
        String buildKey = this.getBuildKey();
        if (buildKey.split("-").length == 3) {
            this.isJob = true;
        }
        else {
            this.isJob = false;
        }

        return result;
    }
    
    /**
     * Init JSON data
     * 
     * @return
     * @throws Exception
     */
    public String doJSON() throws Exception {
    	
        String result = super.doExecute();
        return result;
    }
    
    /**
     * Create JSON based on dynatrace data
     */
    @Override
    public JSONObject getJsonObject() throws JSONException {
    	
    	// Construct empty result object to start with
    	JSONObject jsonObject = super.getJsonObject();

    	// Access config to get DT settings
    	Map<String, String> customBuildData = getImmutablePlan().getBuildDefinition().getCustomConfiguration();
    	
    	try {
    		List<TestRunData> testRuns = BambooDynatraceUtil.getTestRuns(new BambooDynatraceConfiguration(customBuildData));
			
	        List<JSONObject> jsonTimes = getJSONTimes(testRuns);
	        jsonObject.put("testRuns", jsonTimes);
			
		} catch (UnsupportedEncodingException e) {
			this.addErrorMessage("UnsupportedEncodingException while retrieving test run data", e);
		} catch (JDOMException e) {
			this.addErrorMessage("JDOMException while retrieving test run data", e);
		} catch (IOException e) {
			this.addErrorMessage("IOException while retrieving test run data", e);
		}
    	
        return jsonObject;
    }
    
    /**
     * Convert the dynatrace data into JSON
     * 
     * @param testRunsXML
     * @return
     * @throws JSONException
     */
    private List<JSONObject> getJSONTimes(List<TestRunData> testRuns) throws JSONException {
    	    	
    	// Convert to JSON
    	int buildNr = 1;
    	boolean hardNumbers = false;
        List<JSONObject> filteredResult = new ArrayList<JSONObject>();
        for (TestRunData testRun : testRuns) {
        	
    		// Make sure we have a build number
    		if(testRun.getBuildNr()==null || testRun.getBuildNr().length()==0 || hardNumbers) {
    			
    			// Set generated sequence nr
    			testRun.setBuildNr(String.valueOf(buildNr++));
    			
    			// Once we start assigning numbers we need to continue to keep the data proper
    			hardNumbers=true;
    		}
    		
    		// Calc percentage distribution for graph
    		double total = testRun.getNumDegraded()+testRun.getNumVolatile()+testRun.getNumImproved()+testRun.getNumPassed()+testRun.getNumInvalidated()+testRun.getNumFailed();
    		
    		// Skip items with no results at all
    		if(total==0)
    			continue;
    		
    		double degradedPerc = (testRun.getNumDegraded()/total)*100;
    		double volatilePerc = (testRun.getNumVolatile()/total)*100;
    		double improvedPerc = (testRun.getNumImproved()/total)*100;
    		double passedPerc = (testRun.getNumPassed()/total)*100;
    		double invalidPerc = (testRun.getNumInvalidated()/total)*100;
    		double failedPerc = (testRun.getNumFailed()/total)*100;
    		
            JSONObject jsonObject = new JSONObject();
            jsonObject.append("build", testRun.getBuildNr());
            
            jsonObject.append("Degrading", degradedPerc);
            jsonObject.append("Volatile", volatilePerc);
            jsonObject.append("Improved", improvedPerc);
            jsonObject.append("Passing", passedPerc);
            jsonObject.append("Invalidated", invalidPerc);
            jsonObject.append("Failed", failedPerc);
            filteredResult.add(jsonObject);
        }
        
        return filteredResult;
    }

    public boolean getIsJob() {
        return this.isJob;
    }
}
