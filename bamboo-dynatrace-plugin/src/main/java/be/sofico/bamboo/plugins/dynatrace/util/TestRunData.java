package be.sofico.bamboo.plugins.dynatrace.util;

import org.jdom.Element;

/**
 * Capture the test run data in a handy object
 * 
 * @author jasa
 *
 */
public class TestRunData implements Comparable<TestRunData> {

	private int numPassed;
	private int numVolatile;
	private int numImproved;
	private int numDegraded;
	private int numInvalidated;
	private int numFailed;
	
	private String buildNr;
	private long startTime;
	
	public TestRunData() {
		
	}
	
	public TestRunData(Element xml) {
		
		this.numPassed = Integer.parseInt(xml.getAttributeValue("numPassed"));
		this.numVolatile = Integer.parseInt(xml.getAttributeValue("numVolatile"));
		this.numImproved = Integer.parseInt(xml.getAttributeValue("numImproved"));
		this.numDegraded = Integer.parseInt(xml.getAttributeValue("numDegraded"));
		this.numInvalidated = Integer.parseInt(xml.getAttributeValue("numInvalidated"));
		this.numFailed = Integer.parseInt(xml.getAttributeValue("numFailed"));
		this.buildNr = xml.getAttributeValue("versionBuild");
		this.startTime = Long.parseLong(xml.getAttributeValue("startTime"));
	}
	
	public TestRunData(String buildNr, int numPassed, int numVolatile, int numImproved, int numDegraded, int numInvalidated, int numFailed, long startTime) {
	
		this.buildNr = buildNr;
		this.numPassed = numPassed;
		this.numVolatile = numVolatile;
		this.numImproved = numImproved;
		this.numDegraded = numDegraded;
		this.numInvalidated = numInvalidated;
		this.numFailed = numFailed;
		this.startTime = startTime;
	}
	
	public int getNumPassed() {
		return numPassed;
	}
	
	public void setNumPassed(int numPassed) {
		this.numPassed = numPassed;
	}
	
	public int getNumVolatile() {
		return numVolatile;
	}
	
	public void setNumVolatile(int numVolatile) {
		this.numVolatile = numVolatile;
	}
	
	public int getNumImproved() {
		return numImproved;
	}
	
	public void setNumImproved(int numImproved) {
		this.numImproved = numImproved;
	}
	public int getNumDegraded() {
		return numDegraded;
	}
	
	public void setNumDegraded(int numDegraded) {
		this.numDegraded = numDegraded;
	}
	
	public int getNumInvalidated() {
		return numInvalidated;
	}
	
	public void setNumInvalidated(int numInvalidated) {
		this.numInvalidated = numInvalidated;
	}
	
	public String getBuildNr() {
		return buildNr;
	}
	
	public void setBuildNr(String buildNr) {
		this.buildNr = buildNr;
	}
	
	public long getStartTime() {
		return startTime;
	}
	
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}
	
	public int getNumFailed() {
		return numFailed;
	}
	
	public void setNumFailed(int numFailed) {
		this.numFailed = numFailed;
	}
	
	@Override
	public int compareTo(TestRunData o) {
		
		long thisVal = this.startTime;
		long anotherVal = o.startTime;
		return (thisVal<anotherVal ? -1 : (thisVal==anotherVal ? 0 : 1));
	}
	
	@Override
	public String toString() {
		
		StringBuilder b = new StringBuilder();
		b.append("numPassed=").append(numPassed).append(", ")
		.append("numVolatile=").append(numVolatile).append(", ")
		.append("numImproved=").append(numImproved).append(", ")
		.append("numDegraded=").append(numDegraded).append(", ")
		.append("numInvalidated=").append(numInvalidated).append(", ")
		.append("numFailed=").append(numFailed).append(", ")
		.append("buildNr=").append(buildNr).append(", ")
		.append("startTime=").append(startTime);
		
		return b.toString();
	}
}
