package be.sofico.bamboo.plugins.dynatrace;

import java.util.Map;

import be.sofico.bamboo.plugins.dynatrace.util.BambooDynatraceConfiguration;
import be.sofico.bamboo.plugins.dynatrace.util.BambooDynatraceUtil;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.TaskType;

/**
 * A task to register a test session in dynatrace
 * 
 * @author jasa
 *
 */
public class RegisterTestSessionTask implements TaskType {

	@Override
	public TaskResult execute(TaskContext taskContext) throws TaskException {
		
		// Get the logger
		BuildLogger buildLogger = taskContext.getBuildLogger();
		
		// Determine the build number
		String versionBuild = String.valueOf(taskContext.getBuildContext().getBuildNumber());
		
		// Get the configuration
		BambooDynatraceConfiguration config = new BambooDynatraceConfiguration(taskContext.getBuildContext().getParentBuildContext().getBuildDefinition().getCustomConfiguration(), taskContext.getConfigurationMap());
		
        // Are we enabled
        if(!config.isEnabled()) {
        	return TaskResultBuilder.create(taskContext).success().build();
        }
		
		try {
			
			// Register the session
			String dtTestSessionID = BambooDynatraceUtil.registerTestSession(config, config.getCategory(), config.getPlatform(), config.getVersionRevision(), versionBuild, config.getVersionMajor(), config.getVersionMinor());			
			buildLogger.addBuildLogEntry("Dynatrace - Created test session with ID: " + dtTestSessionID);
			
			// Also add as bamboo variable
			Map<String, String> customBuildData = taskContext.getBuildContext().getBuildResult().getCustomBuildData();
			customBuildData.put("dynatrace.testsession.id", dtTestSessionID);
			
		} catch (Exception e) {
    		buildLogger.addErrorLogEntry("Unable to register a test session in Dynatrace: " + e.getMessage(), e);
    		return TaskResultBuilder.create(taskContext).failed().build();
		}
		
		// All went well
		return TaskResultBuilder.create(taskContext).success().build();
	}
}
