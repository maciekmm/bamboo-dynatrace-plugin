package be.sofico.bamboo.plugins.dynatrace.util;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.atlassian.bamboo.build.logger.BuildLogger;

/**
 * Util to access and interact with dynatrace
 * 
 * @author jasa
 *
 */
public class BambooDynatraceUtil {

	/**
	 * Retrieve all registered test runs
	 * 
	 * @param config
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws JDOMException
	 * @throws IOException
	 */
	public static List<TestRunData> getTestRuns(BambooDynatraceConfiguration config) throws UnsupportedEncodingException, JDOMException, IOException {
		
		if(config.getDemo()) {
			
			// Generate sample data
			int buildNumber = 100;
			List<TestRunData> testRuns = new ArrayList<TestRunData>();
			// String buildNr, int numPassed, int numVolatile, int numImproved, int numDegraded, int numInvalidated, int numFailed, long startTime
			testRuns.add(new TestRunData(String.valueOf(buildNumber++), 23, 1, 3, 7, 1, 0, System.currentTimeMillis()));
			testRuns.add(new TestRunData(String.valueOf(buildNumber++), 23, 1, 3, 7, 1, 2, System.currentTimeMillis()));
			testRuns.add(new TestRunData(String.valueOf(buildNumber++), 23, 1, 3, 7, 1, 0, System.currentTimeMillis()));
			testRuns.add(new TestRunData(String.valueOf(buildNumber++), 23, 1, 3, 7, 1, 3, System.currentTimeMillis()));
			testRuns.add(new TestRunData(String.valueOf(buildNumber++), 23, 1, 3, 7, 1, 5, System.currentTimeMillis()));
			testRuns.add(new TestRunData(String.valueOf(buildNumber++), 23, 1, 3, 7, 1, 0, System.currentTimeMillis()));
			testRuns.add(new TestRunData(String.valueOf(buildNumber++), 23, 1, 3, 7, 1, 0, System.currentTimeMillis()));
			testRuns.add(new TestRunData(String.valueOf(buildNumber++), 23, 1, 3, 7, 1, 0, System.currentTimeMillis()));
			testRuns.add(new TestRunData(String.valueOf(buildNumber++), 23, 1, 3, 7, 1, 2, System.currentTimeMillis()));
			testRuns.add(new TestRunData(String.valueOf(buildNumber++), 23, 1, 3, 7, 1, 5, System.currentTimeMillis()));
			
			return testRuns;
			
		} else {
			
			// Init http client
			HttpClient client = new HttpClient();
			
			// Provide authentication settiongs
			client.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(config.getUser(), config.getPassword());
			client.getState().setCredentials(new AuthScope(config.getHost(), config.getPort(), AuthScope.ANY_REALM), defaultcreds);
			
			// Build URL
			String finalUrl = config.getProtocol() + "://" + config.getHost() + ":" + config.getPort() + "/rest/management/profiles/" + config.getProfile() + "/testruns?lastNTestruns=50&extend=testRuns";
			
			// Build GET message
			GetMethod post = new GetMethod(URLEncoder.encode(finalUrl, "UTF-8"));
			post.setDoAuthentication(true);
			
			// Fire away
			int statusCode = client.executeMethod(post);
			Element testRunsXML = null;
			if(statusCode==200 || statusCode==201) {
				testRunsXML = loadElementFromStream(new InputStreamReader(post.getResponseBodyAsStream(), "UTF-8"));			
			}
			
	    	// Generate Results
	    	List<TestRunData> testRuns = new ArrayList<TestRunData>();
	    	
	    	// Parse from XML
	    	if(testRunsXML!=null)  {
	    		Iterator<?> it = testRunsXML.getChildren("testRun").iterator();    	
	        	while (it.hasNext()) {
	    			testRuns.add(new TestRunData((Element)it.next()));
	    		}	
	    	}
	    	
	    	// Sort by time
	    	Collections.sort(testRuns);
	    	
	    	return testRuns;
		}
	}
	
	/**
	 * Get the test run data for this build
	 * 
	 * @param config
	 * @param buildNumber
	 * @param logger 
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws JDOMException
	 * @throws IOException
	 */
	public static TestRunData getTestRunForBuild(BambooDynatraceConfiguration config, int buildNumber, BuildLogger logger) throws UnsupportedEncodingException, JDOMException, IOException {
		
		// Woring in demo mode
		if(config.getDemo()) {
			
			TestRunData sample = new TestRunData(String.valueOf(buildNumber), 23, 1, 3, 0, 1, 5, System.currentTimeMillis());
			return sample;
			
		} else { // Actual call
			
			// Init http client
			HttpClient client = new HttpClient();
			
			// Provide authentication settiongs
			client.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(config.getUser(), config.getPassword());
			client.getState().setCredentials(new AuthScope(config.getHost(), config.getPort(), AuthScope.ANY_REALM), defaultcreds);
			
			// Build URL
			String finalUrl = config.getProtocol() + "://" + config.getHost() + ":" + config.getPort() + "/rest/management/profiles/" + config.getProfile() + "/testruns?short=true&versionBuild=" + buildNumber;
			logger.addBuildLogEntry("DynatraceBuildProcessorServer - Calling Dynatrace on: " + finalUrl);
			
			// Build GET message
			GetMethod post = new GetMethod(URLEncoder.encode(finalUrl, "UTF-8"));
			post.setDoAuthentication(true);
			
			// Fire away
			int statusCode = client.executeMethod(post);
			if(statusCode==200 || statusCode==201) {
				
				logger.addBuildLogEntry("DynatraceBuildProcessorServer - Called Dynatrace successfully HTTP code: " + statusCode);
				Element testRunsXML = loadElementFromStream(new InputStreamReader(post.getResponseBodyAsStream(), "UTF-8"));
				
		    	List<TestRunData> testRuns = new ArrayList<TestRunData>();
		    	
				Iterator<?> it = testRunsXML.getChildren("testRun").iterator();
		    	while (it.hasNext()) {
					testRuns.add(new TestRunData((Element)it.next()));
				}
		    	
		    	if(!testRuns.isEmpty()) {
		    		return testRuns.get(0);
		    	} else {
		    		logger.addBuildLogEntry("DynatraceBuildProcessorServer - No test run data available");
		    	}
			} else {
				logger.addBuildLogEntry("DynatraceBuildProcessorServer - Obtained unsuccessfull HTTP code: " + statusCode);
			}
		}
		
		return null;
	}
	
	/**
	 * Register a test session in dynatrace
	 * 
	 * @param config
	 * @param category
	 * @param platform
	 * @param versionRevision
	 * @param versionBuild
	 * @param versionMajor
	 * @param versionMinor
	 * @return
	 * @throws HttpException
	 * @throws IOException
	 * @throws JDOMException
	 */
	public static String registerTestSession(BambooDynatraceConfiguration config, String category, String platform, String versionRevision, String versionBuild, String versionMajor, String versionMinor) throws HttpException, IOException, JDOMException {
		
		// Demo mode
		if(config.getDemo()) {
			
			return "123456";
			
		} else {
			
			String finalUrl = "http://" + config.getHost() + ":" + config.getPort() + "/rest/management/profiles/" + config.getProfile() + "/testruns";
			
			// Init http client
			HttpClient client = new HttpClient();
			
			// Provide authentication settiongs
			client.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(config.getUser(), config.getPassword());
			client.getState().setCredentials(new AuthScope(config.getHost(), config.getPort(), AuthScope.ANY_REALM), defaultcreds);
			
			// Build POST message
			PostMethod post = new PostMethod(URLEncoder.encode(finalUrl, "UTF-8"));
			post.addRequestHeader("Content-Type", "text/xml");
			post.setDoAuthentication(true);
			
			// Construct XML with request arguments
			Element testRunXML = new Element("testRun");
			testRunXML.setAttribute("category", category);
			if(versionBuild!=null)testRunXML.setAttribute("versionBuild", versionBuild);
			if(platform!=null)testRunXML.setAttribute("platform", platform);
			if(versionRevision!=null)testRunXML.setAttribute("versionRevision", versionRevision);
			if(versionMajor!=null)testRunXML.setAttribute("versionMajor", versionMajor);
			if(versionMinor!=null)testRunXML.setAttribute("versionMinor", versionMinor);
			
			// Add it to the request
			String postData = getStringFromElement(testRunXML);
			post.setRequestEntity(new StringRequestEntity(postData));
			
			// Fire away
			int statusCode = client.executeMethod(post);
			if(statusCode==200 || statusCode==201) {
				
				// Load response into JDOM model
				Element returnXML = loadElementFromStream( new InputStreamReader(post.getResponseBodyAsStream(), "UTF-8"));
				
				// Return the session ID
				return returnXML.getAttribute("id").getValue();
				
			}	
		}
		
		return null;
	}	
	
	/**
	 * Convert the steam to JDOM data
	 * 
	 * @param stream
	 * @return
	 * @throws JDOMException
	 * @throws IOException
	 */
	private static Element loadElementFromStream(InputStreamReader stream) throws JDOMException, IOException {

		try {
			SAXBuilder builder = new SAXBuilder();
			Document doc = builder.build(stream);
			return doc.detachRootElement();
		
		} finally {
			if(stream!=null)
				stream.close();
		}
	}
	
	/**
	 * Convert Element to a String
	 * 
	 * @param xml
	 * @return
	 * @throws IOException
	 */
	public static String getStringFromElement(Element xml) throws IOException {

		XMLOutputter out = new XMLOutputter();
		StringWriter strWriter = new StringWriter();
		out.output(new Document(xml), strWriter);
		return strWriter.toString();
	}
}
