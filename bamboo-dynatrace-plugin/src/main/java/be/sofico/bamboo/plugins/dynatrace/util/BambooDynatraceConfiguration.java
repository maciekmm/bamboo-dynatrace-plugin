package be.sofico.bamboo.plugins.dynatrace.util;

import java.util.Map;

import be.sofico.bamboo.plugins.dynatrace.DynatraceConfigurator;

/**
 * Configuration parameters to access dynatrace
 * 
 * @author jasa
 *
 */
public class BambooDynatraceConfiguration {
	
	// Are we enabled
	private boolean enabled = false;
	
	// Connection Settings
	private String user;
	private String password;
	private String host;
	private Integer port;
	private String profile;
	private Boolean failBuilds;
	private String protocol;
	private Boolean demo;

	// Task Settings
	private String versionMajor;
	private String versionMinor;
	private String versionRevision;
	private String category;
	private String platform;
	
	public BambooDynatraceConfiguration(Map<String, String> buildDefinitionProps) {
		
		this(buildDefinitionProps, null);
	}
	
	public BambooDynatraceConfiguration(Map<String, String> buildDefinitionProps, Map<String, String> taskProps) {
		
		if(buildDefinitionProps==null)
			return;
		
		this.enabled = Boolean.parseBoolean(buildDefinitionProps.get(DynatraceConfigurator.DT_ENABLE));
		this.user = buildDefinitionProps.get(DynatraceConfigurator.DT_USER);
		this.password = buildDefinitionProps.get(DynatraceConfigurator.DT_PASSWORD);
		this.host = buildDefinitionProps.get(DynatraceConfigurator.DT_HOST);
		String tmp = buildDefinitionProps.get(DynatraceConfigurator.DT_PORT);
		if(tmp!=null && tmp.length()>0) {
			this.port = Integer.parseInt(tmp);	
		}
		this.profile = buildDefinitionProps.get(DynatraceConfigurator.DT_PROFILE);
		this.failBuilds = Boolean.parseBoolean(buildDefinitionProps.get(DynatraceConfigurator.DT_FAILBUILDS));
		
		Boolean secure = Boolean.parseBoolean(buildDefinitionProps.get(DynatraceConfigurator.DT_SECURE));
		this.protocol = (secure!=null && secure?"https":"http");
		
		// Load task properties when available
		if(taskProps!=null) {
		
			this.versionMajor = taskProps.get(DynatraceConfigurator.DT_VERSIONMAJOR);
			this.versionMinor = taskProps.get(DynatraceConfigurator.DT_VERSIONMINOR);
			this.versionRevision = taskProps.get(DynatraceConfigurator.DT_VERSIONREVISION);
			this.category = taskProps.get(DynatraceConfigurator.DT_CATEGORY);
			this.platform = taskProps.get(DynatraceConfigurator.DT_PLATFORM);
		}
		
		this.demo = Boolean.parseBoolean(buildDefinitionProps.get(DynatraceConfigurator.DT_DEMO));
	}

	public String getUser() {
		return user;
	}

	public String getPassword() {
		return password;
	}

	public String getHost() {
		return host;
	}

	public int getPort() {
		return port;
	}

	public String getProfile() {
		return profile;
	}

	public Boolean getFailBuilds() {
		return failBuilds;
	}
	
	public String getProtocol() {
		return protocol;
	}

	public String getVersionMajor() {
		return versionMajor;
	}

	public String getVersionMinor() {
		return versionMinor;
	}

	public String getVersionRevision() {
		return versionRevision;
	}

	public String getCategory() {
		return category;
	}

	public String getPlatform() {
		return platform;
	}
	
	public boolean isEnabled() {
		return enabled;
	}
	
	public Boolean getDemo() {
		return demo;
	}
	
	public void setDemo(Boolean demo) {
		this.demo = demo;
	}
}
