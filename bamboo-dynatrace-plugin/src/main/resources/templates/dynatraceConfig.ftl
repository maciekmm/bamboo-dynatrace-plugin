[@ui.bambooSection titleKey="dt-test-automation-configuration.title" ]
        
    [@ww.checkbox labelKey="dt-test-automation-configuration.enable" name='custom.be.sofico.bamboo.plugins.dynatrace.enable' toggle='true' descriptionKey="dt-test-automation-configuration.description" /]
        
    [@ui.bambooSection dependsOn='custom.be.sofico.bamboo.plugins.dynatrace.enable' showOn='true' titleKey="dt-test-automation-configuration.configuration.title"]

		[@ww.textfield name="custom.be.sofico.bamboo.plugins.dynatrace.host" labelKey="dt-test-automation-configuration.host" required='false' descriptionKey="dt-test-automation-configuration.host.description" toggle='true' /]
    	[@ww.textfield name="custom.be.sofico.bamboo.plugins.dynatrace.port" labelKey="dt-test-automation-configuration.port" required='false' descriptionKey="dt-test-automation-configuration.port.description" toggle='true' /]
    	[@ww.checkbox name='custom.be.sofico.bamboo.plugins.dynatrace.secure' toggle='true' labelKey="dt-test-automation-configuration.secure" descriptionKey="dt-test-automation-configuration.failbuilds.secure" /]
    	
    	[@ww.textfield name="custom.be.sofico.bamboo.plugins.dynatrace.user" labelKey="dt-test-automation-configuration.user" required='false' descriptionKey="dt-test-automation-configuration.user.description" toggle='true' /]
    	[@ww.textfield name="custom.be.sofico.bamboo.plugins.dynatrace.password" labelKey="dt-test-automation-configuration.password" required='false' descriptionKey="dt-test-automation-configuration.password.description" toggle='true' /]
    	
    	[@ww.textfield name="custom.be.sofico.bamboo.plugins.dynatrace.profile" labelKey="dt-test-automation-configuration.profile" required='false' descriptionKey="dt-test-automation-configuration.profile.description" toggle='true' /]
    	
    	[@ui.bambooSection titleKey="dt-test-automation-configuration.advanced.title" ]
    		[@ww.checkbox labelKey="dt-test-automation-configuration.failbuilds" name='custom.be.sofico.bamboo.plugins.dynatrace.failbuilds' toggle='true' descriptionKey="dt-test-automation-configuration.failbuilds.description" /]
    	[/@ui.bambooSection]
    	
    	[@ww.checkbox name='custom.be.sofico.bamboo.plugins.dynatrace.demo' toggle='true' labelKey="dt-test-automation-configuration.demo" descriptionKey="dt-test-automation-configuration.demo.description" /]
    	
    [/@ui.bambooSection]
    
[/@ui.bambooSection]